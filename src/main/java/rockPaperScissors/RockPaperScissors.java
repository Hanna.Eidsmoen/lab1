package rockPaperScissors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {

	public static void main(String[] args) {
    	/*
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }


    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            String human_choice = userChoise();
            String computer_choice = computerChoise();
            //   System.out.println("Human chose" + human_choice + ", computer chose " + computer_choice);

            if (isWinner(human_choice, computer_choice)) {
                System.out.println("Human chose " + human_choice + ", computer chose " + computer_choice + ". Human wins!");
                computerScore ++;
            }
            else if ((isWinner(computer_choice,human_choice))) {
                System.out.println("Human chose " + human_choice + ", computer chose " + computer_choice + ". Computer wins.");
                humanScore ++;
            } else {
                System.out.println("Human chose " + human_choice + ", computer chose " + computer_choice + ". It's a tie");
            }
            System.out.println("Score: human " + computerScore + ", computer " + humanScore);
            String continue_answer = continue_playing();
            if (continue_answer.contains("y")) {
                roundCounter ++;
                continue;
            }
            else {
                System.out.println("Bye bye :)");
                break;
        }
        }

    }

    public String userChoise() {
        while (true) {
            String userInput = readInput("Your choice (Rock/Paper/Scissors)?");
            if (rpsChoices.contains(userInput)) {
                return userInput;
            }
            else {
                System.out.printf("I don't understand %s. ", userInput);
            }
        }
    }

        public String continue_playing() {
        //List<String> y_n = Arrays.asList("y", "n");
            while (true) {
                String continue_answer = readInput("Do you wish to continue playing? (y/n)?");
                if (continue_answer.equals("y")) {
                    return "y";
                }
                else if ((continue_answer.equals("n"))){
                    return "n";
                }

               // (validate_input(continue_answer, "y")) {
             //       continue;

                else {
                    System.out.println("I don't understand " + continue_answer + ". Try again");
                }
            }
        }

    public String computerChoise() {
        while (true) {
            String[] compChoice = {"rock", "paper", "scissors"};
            Double randomNumber = Math.random()*3;
            int randomInt = randomNumber.intValue();
            return compChoice[randomInt];
        }
    }
    public Boolean isWinner(String choice1, String choice2) {
        // if ((choice1.equals("paper") && (choice2.equals("rock")))|| (choice1.equals("rock") && choice2.equals("scissors"))
        //|| (choice1.equals("scissors") && (choice2.equals("paper")))); {
        //  return true;
       // if (choice1.equals(choice2));
         //   return

        if (choice1.equals("paper")) {
            return choice2.equals("rock");
        }
        else if ((choice1.equals("scissors"))) {
            return choice2.equals("paper");
        }
        else if ((choice1.equals("rock"))) {
            return choice2.equals("scissors");
        }
        else  {
            return false;
        }
    }

       //else {
         //   return choice2.equals("scissors");
       //}

    public Boolean validate_input(String input, String validInput) {
        return validInput.contains(input);
    }


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */

    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
